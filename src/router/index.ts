import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";

let routes: RouteRecordRaw[] = [

    {
        name: 'Index',
        path: '/index',
        //使用import可以路由懒加载，如果不使用，太多组件一起加载会造成白屏
        component: () => import('@/components/HelloWorld.vue')
    },
   

]

// 路由
const router = createRouter({
    history: createWebHistory(),
    routes
})
// 未登录跳转登录页面
router.beforeEach(async (to, from, next) => {
    const token: String | null = localStorage.getItem('token');
    if (!token && to.path !== '/login') {
        next('/login')
    } else {
        next()
    }
})
// 导出
export default router