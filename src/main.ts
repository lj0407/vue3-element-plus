import { createApp } from 'vue';
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';
import router from "@/router/index";
import App from './App.vue';
import 'lib-flexible'
import "@/utils/rem";
import "@/utils/flexible"

const app = createApp(App)
app.use(router).use(ElementPlus).mount('#app')
