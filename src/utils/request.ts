import axios from 'axios';
import { ElMessage } from 'element-plus';
import { start, close } from '@/utils/nprogress';
import { errorCodeType } from '@/utils/error-code-type';

const base: any = import.meta.env.VITE_API_BASE_URL || '';

const request = axios.create({
    baseURL: base,// 所有的请求地址前缀部分
    // 超时时间 5s
    timeout: 5000,
    withCredentials: true,// 异步请求携带cookie
    headers: {
        // 设置后端需要的传参类型
        'Content-Type': 'application/json',
    },
})


// 请求拦截
request.interceptors.request.use(
    config => {
        start();
        // 每次发送请求之前判断是否存在token
        // 如果存在，则统一在http请求的header都加上token，这样后台根据token判断你的登录情况，此处token一般是用户完成登录后储存到localstorage里的
        let token = localStorage.getItem("token");
        token && (config.headers.Authorization = token)
        // get请求映射params参数
        if (config.method === 'get' && config.params) {
            let url = config.url + '?';
            for (const propName of Object.keys(config.params)) {
                const value = config.params[propName];
                var part = encodeURIComponent(propName) + "=";
                if (value !== null && typeof (value) !== 'undefined') {
                    // 对象处理
                    if (typeof value === 'object') {
                        for (const key of Object.keys(value)) {
                            let params = propName + '[' + key + ']';
                            var subPart = encodeURIComponent(params) + "=";
                            url += subPart + encodeURIComponent(value[key]) + "&";
                        }
                    } else {
                        url += part + encodeURIComponent(value) + "&";
                    }
                }
            }
            url = url.slice(0, -1);
            config.params = {};
            config.url = url;
        }
        return config
    },
    error => {
        close();
        // 对请求错误做些什么
        Promise.reject(error)
    }
)

// 响应拦截器
request.interceptors.response.use(
    (res: any) => {
        start();
        // 未设置状态码则默认成功状态
        const code = res.data['code'] || 200;
        // 获取错误信息
        const msg = errorCodeType(code) || res.data['msg'] || errorCodeType('default');
        if (code === 200) {
            return Promise.resolve(res.data)
        } else {
            ElMessage.error(msg)
            return Promise.reject(res.data)
        }
    },
    error => {
        close();
        // 对响应错误做点什么
        let { response, message } = error
        if (message == "Network Error") {
            message = "后端接口连接异常";
        }
        else if (message.includes("timeout")) {
            message = "系统接口请求超时";
        }
        else if (message.includes("Request failed with status code")) {
            message = "系统接口" + message.substr(message.length - 3) + "异常";
        }
        ElMessage.error({
            message: message,
            duration: 5 * 1000
        })
        return Promise.reject(error)
    }
)


export function get(url: string, params: any) {
    return request.get(url, { params })
}

export function post(url: string, data: any) {
    return request.post(url, data)
}

export default request